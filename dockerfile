FROM debian
RUN apt-get update
RUN apt-get install -y git
RUN mkdir /root/.ssh/
RUN touch /root/.ssh/known_hosts
COPY id_rsa /root/.ssh/id_rsa
RUN chmod 700 /root/.ssh/id_rsa
RUN chown -R root:root /root/.ssh
RUN ssh-keyscan bitbucket.org >> /root/.ssh/known_hosts
RUN git clone git@bitbucket.org:asadinteg321/dellrepo.git
RUN cd dellrepo
WORKDIR /dellrepo
RUN git pull origin master
RUN apt-get install -y python-pip python-dev python3-pip
RUN pip3 install flask
RUN pip3 install celery
ENTRYPOINT [ "python3" ]
CMD [ "app.py" ]




